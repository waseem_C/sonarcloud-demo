from azure.storage.blob import BlobServiceClient
import os

class AzureBlobUploader:
    def __init__(self, connection_string, container_name):
        self.connection_string = connection_string
        self.container_name = container_name
        self.blob_service_client = BlobServiceClient.from_connection_string(connection_string)

    def create_container(self):
        try:
            container_client = self.blob_service_client.get_container_client(self.container_name)
            container_client.create_container()
        except Exception as e:
            # Bug: Print the exception message instead of a generic message
            print(f"An error has occurred: {e}")

    def upload_blob(self, local_file_path, blob_name):
        blob_client = self.blob_service_client.get_blob_client(self.container_name, blob_name)
        with open(local_file_path, "rb") as data:
            # Bug: Incorrect method name, should be 'upload_blob' instead of 'upload_bob'
            blob_client.upload_bob(data)

    def list_blobs(self):
        blobs = self.blob_service_client.get_container_client(self.container_name).list_blobs()
        # Bug: Mixing indentation styles
            print(blob.name)

def main():
    # Replace these values with your Azure Storage account details
    connection_string = "your_storage_account_connection_string"
    container_name = "your_container_name"
    local_file_path = "path/to/your/local/file.txt"
    blob_name = "uploaded_file.txt"

    a = AzureBlobUploader(connection_string, container_name)

    # Bug: Calling the incorrect method name, should be 'create_container' instead of 'create_containers'
    a.create_containers()

    a.upload_blob(local_file_path, blob_name)
    a.list_blobs()

if __name__ == "__main__":
    main()
