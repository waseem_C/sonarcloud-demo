# sample_application.py

# Azure Python application with intentional code smells

from azure.storage.blob import BlobServiceClient
import os

class AzureBlobUploader:
    def __init__(self, connection_string, container_name):
        self.connection_string = connection_string
        self.container_name = container_name
        self.blob_service_client = BlobServiceClient.from_connection_string(connection_string)

    def create_container(self):
        # Code smell: Unused variable
        unused_variable = "This variable is not used"

        # Code smell: Using 'except:' without specifying the exception type
        try:
            container_client = self.blob_service_client.get_container_client(self.container_name)
            container_client.create_container()
        except:
            print("An error occurred while creating the container.")

    def upload_blob(self, local_file_path, blob_name):
        # Code smell: Unnecessary comments
        # This function uploads a blob to Azure Storage

        # Code smell: Function does not return anything
        blob_client = self.blob_service_client.get_blob_client(self.container_name, blob_name)
        with open(local_file_path, "rb") as data:
            blob_client.upload_blob(data)

    def list_blobs(self):
        # Code smell: Function name does not convey its purpose clearly
        blobs = self.blob_service_client.get_container_client(self.container_name).list_blobs()
        print("Blobs in the container:")
        for blob in blobs:
            # Code smell: Mixing indentation styles
                print(blob.name)

def main():
    # Replace these values with your Azure Storage account details
    connection_string = "your_storage_account_connection_string"
    container_name = "your_container_name"
    local_file_path = "path/to/your/local/file.txt"
    blob_name = "uploaded_file.txt"

    # Code smell: Using global variables
    global_variable = "This is a global variable"

    # Code smell: Using a single letter for variable names
    a = AzureBlobUploader(connection_string, container_name)

    # Code smell: Function is not being called
    a.create_container()

    # Code smell: Repeating the same code
    a.create_container()

    # Code smell: Function is not being called
    a.list_blobs()

if __name__ == "__main__":
    main()

from azure.storage.blob import BlobServiceClient
import os

class AzureBlobUploader:
    def __init__(self, connection_string, container_name):
        self.connection_string = connection_string
        self.container_name = container_name
        self.blob_service_client = BlobServiceClient.from_connection_string(connection_string)

    def create_container(self):
        try:
            container_client = self.blob_service_client.get_container_client(self.container_name)
            container_client.create_container()
        except Exception as e:
            # Security Hotspot: Logging sensitive information (exception details) may expose vulnerabilities
            print(f"An error occurred: {e}")

    def upload_blob(self, local_file_path, blob_name):
        blob_client = self.blob_service_client.get_blob_client(self.container_name, blob_name)
        with open(local_file_path, "rb") as data:
            # Security Hotspot: Uploading untrusted user data without proper validation and sanitization
            blob_client.upload_blob(data)

    def list_blobs(self):
        blobs = self.blob_service_client.get_container_client(self.container_name).list_blobs()
        for blob in blobs:
            # Security Hotspot: Logging sensitive information (blob names) may expose vulnerabilities
            print(blob.name)

def main():
    # Security Hotspot: Hardcoding sensitive information (e.g., connection string) directly in the code
    connection_string = "your_storage_account_connection_string"
    container_name = "your_container_name"
    local_file_path = "path/to/your/local/file.txt"
    blob_name = "uploaded_file.txt"

    a = AzureBlobUploader(connection_string, container_name)

    a.create_container()
    a.upload_blob(local_file_path, blob_name)
    a.list_blobs()

if __name__ == "__main__":
    main()

