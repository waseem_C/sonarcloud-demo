# Code Smell Example

class Calculator:
    def __init__(self):
        pass

    # Code Smell: God function (doing too much)
    def perform_complex_calculation(self, a, b, c, d):
        result = self._step1(a, b)
        result = self._step2(result, c)
        result = self._step3(result, d)
        return result

    def _step1(self, a, b):
        return a + b

    def _step2(self, result, c):
        # Code Smell: Magic number
        return result * c + 42

    def _step3
